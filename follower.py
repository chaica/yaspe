'''Follower mapping for SQLAlchemy'''
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, Integer, String, DateTime, Boolean

MYBASE = declarative_base()

class Follower(MYBASE):
    '''Follower mapping for SQLAlchemy'''
    __tablename__ = 'follower'

    twitter_id = Column(Integer, primary_key=True)
    screen_name = Column(String)
    following = Column(Boolean)
    unfollowed = Column(Boolean)
    ifollowhim = Column(Boolean)
    added_at = Column(DateTime)
    fewfollowers = Column(Boolean)
    initial = Column(Boolean)
    to_add_later = Column(Boolean)
