'''CountMyFollowers class to register the current followers'''

from time import sleep
from random import randint

import tweepy

class CountMyFollowers(object):
    '''CountMyFollowers class'''
    def __init__(self, twconn, session):
        '''Constructor of the CountMyFollowers class'''
        self.twconn = twconn
        self.flrslist = []
        myself = self.twconn.connection().me().id
        for flr in tweepy.Cursor(self.twconn.connection().followers, id=myself).items():
            self.flrslist.append(flr.id)
            print('countmyfollowers - {} nous suit'.format(flr.screen_name))
            sleep(randint(3, 5))

    def followerslist(self):
        '''return the list of our followers'''
        return self.flrslist
