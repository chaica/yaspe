'''Initial class for the initial import'''

from datetime import datetime
from random import randint
from time import sleep

from follower import Follower
import tweepy

class Initial(object):
    '''Initial count of the followers'''
    def __init__(self, twconn, session):
        '''Constructor of the Initial class'''
        print('Initial synchronization!')
        self.twconn = twconn
        self.flrslist = []
        follow = True
        myself = self.twconn.connection().me().id
        for flr in tweepy.Cursor(self.twconn.connection().followers, id=myself).items():
            self.flrslist.append(flr.id)
            # search if the user already exists in the db (should not but, hey, who knows...)
            userquery = session.query(Follower).filter(Follower.twitter_id == flr.id)
            if userquery.count() > 0:
                userindb = True
                dbuser = userquery[0]
                # we already unfollowed this user, not following again
                if dbuser.unfollowed:
                    follow = False
                else:
                    follow = True
            else:
                userindb = False

            # check if the follower has few followers (we can unfollow it discretly if so
            if flr.friends_count > 11 and flr.friends_count < 30:
                fewfollowers = True
            else:
                fewfollowers = False

            # add the user in the database
            if not userindb and follow:
                twitteruser = Follower(
                    twitter_id=flr.id, screen_name=flr.screen_name,
                    following=True, unfollowed=False,
                    added_at=datetime.now(), fewfollowers=fewfollowers, initial=True, to_add_later=False)
                session.add(twitteruser)
                session.commit()
                print('initial synchro added: {} id:{} at {}'.format(
                    flr.screen_name, flr.id, str(twitteruser.added_at)))
            sleep(randint(3, 5))

    def followerslist(self):
        '''return the list of our followers'''
        return self.flrslist
