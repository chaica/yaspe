'''Clean the unneeded Twitter friends'''
from datetime import datetime, timedelta
from follower import Follower

# our own modules
from waitamoment import WaitAMoment

class Clean:
    '''Clean the unneeded Twitter friends'''
    def __init__(self, twconn, session, followerslist):
        '''Constructor of the Clean class'''
        self.twconn = twconn
        fewfollowerscount = 20
        for dbfollower in session.query(Follower).filter_by(unfollowed=False):
            if (dbfollower.twitter_id not in followerslist) and dbfollower.following and (dbfollower.added_at < (datetime.now() - timedelta(1))) and not dbfollower.to_add_later:
                self.twconn.connection().get_user(dbfollower.twitter_id).unfollow()
                dbfollower.unfollowed = True
                session.commit()
                print('unfollowed: {} id:{} - not following me any more'.format(dbfollower.screen_name, dbfollower.twitter_id))
                WaitAMoment()
            if dbfollower.fewfollowers and fewfollowerscount > 0 and (dbfollower.added_at < (datetime.now() - timedelta(1))) and not dbfollower.to_add_later:
                fewfollowerscount -= 1
                self.twconn.connection().get_user(dbfollower.twitter_id).unfollow()
                dbfollower.unfollowed = True
                session.commit()
                print('unfollowed: {} id:{} - too few followers'.format(dbfollower.screen_name, dbfollower.twitter_id))
                WaitAMoment()
