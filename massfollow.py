'''MassFollow class to get new users'''

from datetime import datetime, timedelta
from random import randint, shuffle
from time import sleep
from follower import Follower

import tweepy

# our own modules
from addstoredfollowers import AddStoredFollowers
from waitamoment import WaitAMoment
from getkeywords import GetKeywords

class MassFollow(object):
    '''MassFollow class'''
    def __init__(self, twconn, session):
        '''MassFollow class constructor'''
        self.twconn = twconn
        self.flrslist = []
        self.storeforlater = 0
        outoftheloop = False
        # lets add the stored gonna be followers
        addmystoredfollowers = AddStoredFollowers(self.twconn, session)
        self.flrslist += addmystoredfollowers.followerslist()
        dbtwittos = session.query(Follower, Follower.twitter_id).all()
        twittos = [i.twitter_id for i in dbtwittos]
        shuffle(twittos)
        for userid in twittos:
            print(self.twconn.connection().rate_limit_status())
            for flr in tweepy.Cursor(self.twconn.connection().followers, id=userid).items():
                print('massfollow - on regarde {}'.format(flr.screen_name))
                follow = True
                # apply tests for already found users
                userquery = session.query(Follower).filter(Follower.twitter_id == flr.id)
                if userquery.count() > 0:
                    userindb = True
                    dbuser = userquery[0]
                    # we already unfollowed this user, not following again
                    if dbuser.unfollowed:
                        follow = False
                else:
                    userindb = False
                #
                # lets test if it is a good follower or not!
                #
                # if the lang is different from english and french
                if flr.lang != 'fr' and flr.lang != 'en':
                    follow = False
                # if the user description is empty, seems like a unused or fake account
                if flr.description == "":
                    follow = False
                # find out if user description is interesting
                match = False
                # retrieve good keywords and bad keywords
                getthem = GetKeywords()
                okwords = getthem.okwords
                nokwords = getthem.nokwords

                for keyword in okwords:
                    if keyword in flr.description.lower():
                        match = True
                if not match:
                    follow = False
                # don't follow the user with description haveing thoses words
                match = False
                for keyword in nokwords:
                    if keyword in flr.description.lower():
                        match = True
                if match:
                    follow = False
                # if the user does not have friends, it's a fake account
                if flr.friends_count <= 10:
                    follow = False
                if flr.friends_count > 11 and flr.friends_count < 30:
                    fewfollowers = True
                else:
                    fewfollowers = False
                # if the user is protected, he won't add me
                if flr.protected:
                    follow = False
                # if the user was just created, it's a fake account
                if flr.created_at > (datetime.now() - timedelta(30)):
                    follow = False
                # if follower does not follow people enough, just ignore it
                if flr.followers_count > 0:
                    if flr.friends_count / flr.followers_count < 0.1:
                        follow = False

                addit = True
                #lets add and follow the guy if possible
                if not userindb and follow:
                    self.twconn.update_connection()
                    if self.twconn.connection().me().followers_count < 1000:
                        if self.twconn.connection().me().friends_count <= self.twconn.connection().me().followers_count:
                            addit = True
                        else:
                            addit = False
                    if self.twconn.connection().me().followers_count > 1000:
                        if self.twconn.connection().me().friends_count <= (self.twconn.connection().me().followers_count + 150):
                            addit = True
                        else:
                            addit = False
                    if addit:
                        # let's follow this guy!
                        flr.follow()
                        # let's add it in the db
                        twitteruser = Follower(
                            twitter_id=flr.id, screen_name=flr.screen_name,
                            following=True, unfollowed=False, added_at=datetime.now(), fewfollowers=fewfollowers, initial=False, to_add_later=False)
                        session.add(twitteruser)
                        session.commit()
                        # updating the list of the followers
                        self.flrslist.append(flr.id)
                        print('followed: {} id:{} at {}'.format(
                            flr.screen_name, flr.id, str(twitteruser.added_at)))
                        # let's sleep a while before the next action
                        WaitAMoment()
                    else:
                        # store the twitter id to add it later
                        twitteruser = Follower(
                            twitter_id=flr.id, screen_name=flr.screen_name,
                            following=True, unfollowed=False, added_at=datetime.now(), fewfollowers=fewfollowers, initial=False, to_add_later=True)
                        session.add(twitteruser)
                        session.commit()
                        # updating the list of the followers
                        self.flrslist.append(flr.id)
                        print('stored for later: {} id:{} at {}'.format(
                            flr.screen_name, flr.id, str(twitteruser.added_at)))
                        self.storeforlater += 1
                    if self.storeforlater == 100:
                        outoftheloop = True
                sleep(randint(3, 5))
                if outoftheloop:
                    break
            if outoftheloop:
                break

    def followerslist(self):
        '''return the list of our followers'''
        return self.flrslist
