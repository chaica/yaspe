'''Wait a moment before going on'''
import datetime
import time
from random import randint

class WaitAMoment(object):
    '''Wait a moment before going on'''
    def __init__(self):
        '''Constructor of the WaitAMoment class'''
        ignorefirstwait = False
        while self.stopandwait():
            ignorefirstwait = True
            self.wait()
        # if we just waited, don't wait again
        if not ignorefirstwait:
            self.wait()

    def stopandwait(self):
        res = False
        # simulate human sleep and lunch break
        if (datetime.datetime.now().hour > 2 and \
            datetime.datetime.now().hour < 7) or \
            datetime.datetime.now().hour == 12:
            res = True
        return res

    def wait(self):
        time.sleep(60)
        waitsec = randint(0, 60)
        time.sleep(waitsec)
