'''AddNowNewFollower class to immediatly store a new follower'''

from datetime import datetime, timedelta
from follower import Follower
from random import randint, shuffle
from time import sleep

import tweepy

# our own modules
from follower import Follower
from waitamoment import WaitAMoment

class AddNowNewFollower(object):
    '''AddNowNewFollower class'''
    def __init__(self, twconn, session):
        '''AddNowNewFollower class constructor'''
        if addit:
            # let's add it in the db
            twitteruser = Follower(
                twitter_id=flr.id, screen_name=flr.screen_name,
                following=True, ifollowhim=True, unfollowed=False, added_at=datetime.now(), fewfollowers=fewfollowers, initial=False, to_add_later=False)
            session.add(twitteruser)
            session.commit()
            print('immediatly followed: {} id:{} at {}'.format(
                flr.screen_name, flr.id, str(twitteruser.added_at)))
            # let's sleep a while before the next action
            WaitAMoment()
