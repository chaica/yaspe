'''Get the keywords to search in the twitter description'''
import configparser
import os.path
import sys

class GetKeywords:
    '''Get the keywords to search in the twitter description'''
    def __init__(self):
        '''Constructor of the GetKeywords class'''
        self.okkeywords = ''
        self.nokkeywords = ''
        pathtoconf = sys.argv[-1]
        if not os.path.exists(pathtoconf):
            print('the path you provided for yaspe configuration file does not exists')
            sys.exit(1)
        if not os.path.isfile(pathtoconf):
            print('the path you provided for yaspe configuration is not a file')
            sys.exit(1)
        __config = configparser.ConfigParser()
        try:
            with open(pathtoconf) as __conffile:
                __config.read_file(__conffile)
                if __config.has_section('main'):
                    self.okkeywords = __config['main']['ok']
                    self.nokkeywords = __config['main']['nok']
        except (configparser.Error, IOError, OSError) as __err:
            print(__err)
            sys.exit(1)
        self.okkeywords = self.okkeywords.split(',')
        self.nokkeywords = self.nokkeywords.split(',')

    @property
    def okwords(self):
        return self.okkeywords

    @property
    def nokwords(self):
        return self.nokkeywords
