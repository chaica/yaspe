#pylint: skip-file
'''Main of Yaspe'''
import sys

from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from sqlalchemy.ext.declarative import declarative_base

import tweepy

# our own modules
from clean import Clean
from countmyfollowers import CountMyFollowers
from addstoredfollowers import AddStoredFollowers
from initial import Initial
from massfollow import MassFollow
from twitter import Twitter
from follower import Follower
from waitamoment import WaitAMoment

def getfollowerstoaddlater(thissession):
    '''get all followers waiting to be added'''
    dbwannabetwittos = thissession.query(Follower).filter_by(to_add_later=True)
    return dbwannabetwittos.count()


class Main(object):
    '''Main of Yaspe'''
    def __init__(self):
        '''Main of Yaspe'''
        print('Yaspe starts!')
        base = declarative_base()

        # sqlalchemy connector
        engine = create_engine('sqlite:///yaspe.db')
        tmpsession = sessionmaker(bind=engine)
        session = tmpsession()
        Follower.metadata.create_all(engine)
        myfollowers = []
        starting = True

        # initial synchronisation
        twconn = Twitter()
        if not session.query(Follower).first():
            myinit = Initial(twconn, session)
            myfollowers = myinit.followerslist()
        else:
            mycount = CountMyFollowers(twconn, session)
            myfollowers = mycount.followerslist()
        while True:
            twconn.update_connection()
            try:
                if getfollowerstoaddlater(session) < twconn.connection().me().followers_count:
                    # search and add followers
                    mfme = MassFollow(twconn, session)
                    # update myfollowers list with new followers
                    myfollowers += mfme.followerslist()
                    starting = False
                # clean the non-following twittos
                Clean(twconn, session, myfollowers)
                # update the followers list
                # if the app just starts don't parse again our followers
                if starting:
                    starting = False
                else:
                    mycount = CountMyFollowers(twconn, session)
                    myfollowers = mycount.followerslist()
                addmystoredfollowers = AddStoredFollowers(twconn, session)
                myfollowers += addmystoredfollowers.followerslist()
            except KeyboardInterrupt as msg:
                print(msg)
                sys.exit(0)
            except tweepy.error.TweepError as msg:
                print(msg)
                WaitAMoment()
