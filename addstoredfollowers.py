'''Add the previously stored followers'''
from datetime import datetime
from follower import Follower
from random import randint
from time import sleep

# our own modules
from waitamoment import WaitAMoment


class AddStoredFollowers(object):
    '''Add the previously stored followers'''
    def __init__(self, twconn, session):
        '''Constructor of the AddStoredFollowers class'''
        self.flrslist = []
        # lets add the stored gonna be followers
        dbwannabetwittos = session.query(Follower).filter_by(to_add_later=True)
        if dbwannabetwittos:
            for dbwannabetwitto in dbwannabetwittos:
                twconn.update_connection()
                if twconn.connection().me().followers_count < 1000:
                    if twconn.connection().me().friends_count <= twconn.connection().me().followers_count:
                        addguys = True
                    else:
                        break
                if twconn.connection().me().followers_count > 1000:
                    if twconn.connection().me().friends_count <= (twconn.connection().me().followers_count + 150):
                        addguys = True
                    else:
                        break
                if addguys:
                    # lets follow that guy!
                    twconn.connection().get_user(dbwannabetwitto.twitter_id).follow()
                    # let's add it in the db
                    dbwannabetwitto.added_at = datetime.now()
                    dbwannabetwitto.to_add_later = False
                    session.commit()
                    # updating the list of the followers
                    self.flrslist.append(dbwannabetwitto.twitter_id)
                    print('addstoredfollowers - followed: id:{}'.format(
                        dbwannabetwitto.twitter_id))
                    WaitAMoment()
                sleep(randint(3, 5))

    def followerslist(self):
        '''return the list of our followers'''
        return self.flrslist

